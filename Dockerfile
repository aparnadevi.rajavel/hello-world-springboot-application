FROM openjdk:11

COPY build/libs/hello-world-0.1.0.jar hello-world.jar

EXPOSE 8080

ENTRYPOINT ["java","-jar","/hello-world.jar"]